---
  title: Plan
  description: Learn how GitLab provides powerful planning tools to keep everyone synchronized. View more here!
  components:
    - name: sdl-cta
      data:
        title: Plan
        aos_animation: fade-down
        aos_duration: 500
        subtitle: Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
        text: |
          GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress. Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises. GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.
        icon:
          name: plan-alt-2
          alt: Plan Spotlight Icon
          variant: marketing
    - name: featured-media
      data:
        column_size: 6
        header: Product categories
        header_animation: fade-up
        header_animation_duration: 500
        media:
          - title: Team Planning
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Plan, organize, and track team progress using Scrum, Kanban, SAFe, and other Agile methodologies.
            link:
              href: https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning
              text: Learn More
              data_ga_name: team planning learn more
              data_ga_location: body
          - title: Portfolio Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Plan upcoming work by creating Epics and mapping all relevant Issues to them. Create and track against multiple milestones at the portfolio level to see status overtime and review progress towards your goals
            link:
              href: /stages-devops-lifecycle/portfolio-management/
              text: Learn More
              data_ga_name: portfolio management learn more
              data_ga_location: body
          - title: Service Desk
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Connect your team using GitLab issues, to external parties directly via email for feedback and support, with no additional tools required.
            link:
              href: /stages-devops-lifecycle/service-desk/
              text: Learn More
              data_ga_name: service desk learn more
              data_ga_location: body
          - title: Requirements Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Gather and manage the use cases and requirements to meet business objectives.
            link:
              href: https://docs.gitlab.com/ee/user/project/requirements/
              text: Learn More
              data_ga_name: requirements management learn more
              data_ga_location: body
          - title: Quality Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Plan and track testing and quality of your product.
            link:
              href: https://docs.gitlab.com/ee/ci/test_cases/index.html
              text: Learn More
              data_ga_name: quality management learn more
              data_ga_location: body
          - title: Design Management
            aos_animation: fade-up
            aos_duration: 500
            shadow_hover: true
            text: |
              Upload design assets to GitLab issues for easy collaboration on designs with a single source of truth.
            link:
              href: https://docs.gitlab.com/ee/user/project/issues/design_management.html
              text: Learn More
              data_ga_name: design management learn more
              data_ga_location: body
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - align_center: true
            text: |
              Learn more about our roadmap for upcoming features on our [Direction page](/direction/plan){data-ga-name="plan direction" data-ga-location="body"}.
    - name: sdl-related-card
      data:
        column_size: 4
        header: Related
        cards:
          - title: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 500
            text: Create, view, manage code and project data through powerful branching tools.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/create/
              data_ga_name: create
              data_ga_location: body
          - title: Verify
            icon:
              name: verify-alt-2
              alt: Verify Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1000
            text: |
              Keep strict quality standards for production code with automatic testing and reporting.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/verify/
              data_ga_name: verify
              data_ga_location: body
          - title: Manage
            icon:
              name: manage-alt-2
              alt: Manage Icon
              variant: marketing
            aos_animation: zoom-in
            aos_duration: 1500
            text: |
              Gain visibility and insight into how your business is performing.
            link:
              text: Learn More
              href: /stages-devops-lifecycle/manage/
              data_ga_name: manage
              data_ga_location: body
