
# Automated semantic versioning, changelogs and tag creation with `semantic-release`

## Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) Is a common library that allows to create, automate and release 
any package using [Semantic Versioning](https://semver.org/).

For Buyers Experience, some features from `Semantic Release` are used in order to automate the release
process and have a proper changelog file and proper release tags.

The configuration for `Semantic Release` can be found in [package.json](/package.json) file and the configuration is similar as shown below:

```
"release": {
    "branches": [
      "main"
    ],
    "plugins": [
      "@semantic-release/commit-analyzer",
      "@semantic-release/release-notes-generator",
      "@semantic-release/npm",
      [
        "@semantic-release/changelog",
        {
          "changelogFile": "CHANGELOG.md"
        }
      ],
      [
        "@semantic-release/git",
        {
          "assets": [
            "CHANGELOG.md",
            "package.json"
          ],
          "message": "Release: ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
        }
      ]
    ],
    "preset": "conventionalcommits"
  }
```

The order of the plugins is very important for the process and the job to work correctly, 
the configuration for Buyers Experience is explained below:

1. **@semantic-release/commit-analyzer**: This plugin analyzes every commit since last release and determines if the new version is a MAJOR, MINOR or PATCH. This plugin works base on a commit message convention.
For this project, the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) preset is used.
2. **@semantic-release/release-notes-generator**: This plugin works together with the `commit-analyzer`, depending on the results, it will generate the notes for the tag and the changelog.
3. **@semantic-release/npm**: This plugin is the one in charge to update the npm version ([package.json](/package.json)) and also publish the new version in npm. In this case, the project will not be published in npm as the project is marked as `private:true` in the npm configuration.
4. **@semantic-release/changelog**: This plugin will create or update the [CHANGELOG.md](/CHANGELOG.md) file based on the output from the previous plugins.
5. **@semantic-release/git**: This plugin will commit into the selected branch the files modified by the previous plugins, in this case, the `package.json` and `CHANGELOG.md` files.

## Conventional Commits

It is really important for the reviewers to use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) in order to maintain consistency and to have good changelog generation in the project. These are some considerations to have in mind when merging a
merge request into the main branch:

1. **Types**: In order to determine if the next release will be a MAJOR, MINOR or PATCH release, in a merge request commit message, the `type` must be specified using one of the options below:
   - MAJOR types:
     - Using `!` next to any type
     - Using `BREAKING:` in the footer of the message
   - MINOR types: 
     - feat
     - chore
   - PATCH types:
     - fix
     - docs
     - refactor
     - style
     - test


2. **Commit Messages:** The template for `conventional commits` is

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

An example of a breaking change(MAJOR) release message is:

```
 feat(Solutions)!: New solutions page added
```

```
 feat(Solutions): New solutions page added

 BREAKING: The solutions template has a new prop
```

With one of those commit messages, the MAJOR version will be raised (i.e 1.2.0 to 2.0.0)

3. Before merging any `Merge Request` into `main`, the commits must be squashed into one single commit
and the default merge message must be edited using `Conventional Commits` following the previous steps.

## CI/CD Job process

The process to release a new version and generate the changelog file is:

1. A manual job is triggered at the end of each iteration, it will run all the plugins from `Semantic Release`
2. When the job is finished, new commits will be added to the `main` branch with the changes in the `package.json` and `CHANGELOG.md` files. 
These commits will not trigger any pipeline.
3. When the job is finished, a new tag will be created with `Semantic Versioning` naming convention (i.e v1.0.0) and this tag will contain all changes made since last release/iteration

# How to run semantic-release locally (Dry Run Mode)

In order to test `Semantic Release` locally, these are some steps to be followed:

1. Change the branch name to the local branch name where the test will be run in the [package.json](package.json).
```
  ...
  "release": {
    "branches": [
      "your_branch_name"
    ],
  ...
```
2. Create some test commits in the local branch using [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
3. Run `yarn semantic-release --dry-run`. 
Dry Run mode allows running `Semantic Release` locally and skips the following steps: prepare, publish, success and fail. 
In addition to this it prints the next version and release notes from the test commits to the console,
allowing the developer to see the new version and changelog without modifying the files.
